### TODO: Replace this with a test that uses LAS point cloud
# @testset "Test height map from pointcloud" begin

#     currentfolder = dirname(@__FILE__())
#     test_files = joinpath(currentfolder, "test_files")

#     tileXmin = 560500
#     tileYmin = 3965900
#     tile_name = "32615_g2012bu0_560500_3965900"

#     if isfile(joinpath(test_files, "img", "$(tile_name).h5"))
#         rm(joinpath(test_files, "img", "$(tile_name).h5"))
#     end

#     if isfile(joinpath(test_files, "img", "$(tile_name).png"))
#         rm(joinpath(test_files, "img", "$(tile_name).png"))
#     end

#     pc = load_pointcloud(joinpath(test_files, "pc","$(tile_name).h5"))

#     save_dir =  joinpath(test_files, "img")
#     filename = generate_height_map(pc, tileXmin, tileYmin, tile_name; save_dir = save_dir)

#     @test isfile(joinpath(test_files, "img", "$(tile_name).png"))
#     @test isfile(joinpath(test_files, "img", "$(tile_name).h5"))

# end

@testset "Test denoising" begin

    dense_points = map(i -> Vector3(rand(3)), 1:10)
    sparse_points = map(i -> 200 * Vector3(rand(3)), 1:10)

    threshold = 2.0

    all_points = shuffle(cat(dense_points, sparse_points, dims=1))
    brute_force_noise_mask = falses(length(all_points))

    for i in 1:length(all_points)
        this_point = all_points[i]
        distances = sort(map(p -> norm(p - this_point), all_points))
        not_noise = distances[2] < threshold
        brute_force_noise_mask[i] = not_noise
    end

    denoised = HeightMaps.denoise_mask(all_points, threshold)

    @test all(denoised .== brute_force_noise_mask)
    
end

@testset "Pixel Indexing" begin
    point2 = Point2(0, 0)
    point3 = Point3(0, 0, 1)
    xmin = -5
    ymax = 5
    zmin = 0
    metres_per_pixel = 1
    @test_throws ArgumentError HeightMaps.get_pixel_index(point2, xmin, ymax, 0)
    @test_throws ArgumentError HeightMaps.get_pixel_index(point2, xmin, ymax, -1)

    @test CartesianIndex(5, 6) == HeightMaps.get_pixel_index(point2, xmin, ymax, 1)
    @test CartesianIndex(3, 3) == HeightMaps.get_pixel_index(point2, xmin, ymax, 2)
    @test CartesianIndex(10, 11) == HeightMaps.get_pixel_index(point2, xmin, ymax, 0.5)
    # check 3 dimensions
    @test CartesianIndex(5, 6, 2) == HeightMaps.get_pixel_index(point3, xmin, ymax, zmin, 1)
    @test CartesianIndex(3, 3, 1) == HeightMaps.get_pixel_index(point3, xmin, ymax, zmin, 2)
    @test CartesianIndex(10, 11, 3) == HeightMaps.get_pixel_index(point3, xmin, ymax, zmin, 0.5)
end

@testset "mapwindow" begin
    points = map(i -> Point3(rand(3)), 1:100)
    pc = Table(position = points)
    binned_pc = bin_pointcloud(pc, 0.1; num_dims = 2)
    @testset "min/max filters" begin
        @test_throws ArgumentError max_filter(binned_pc, 4)
        max_filtered = max_filter(binned_pc, 3)
        binned_pc_max_x_idx = minimum(map(idx -> idx[1], collect(keys(binned_pc))))
        binned_pc_max_y_idx = minimum(map(idx -> idx[2], collect(keys(binned_pc))))
        max_filtered_max_x_idx = minimum(map(idx -> idx[1], collect(keys(max_filtered))))
        max_filtered_max_y_idx = minimum(map(idx -> idx[1], collect(keys(max_filtered))))

        @test binned_pc_max_x_idx == max_filtered_max_x_idx
        @test binned_pc_max_y_idx == max_filtered_max_y_idx
        for idx ∈ keys(binned_pc)
            # check the max function was applied correctly to each voxel
            @test max_filtered[idx][1] == maximum(map(p -> p.position[3], binned_pc[idx]))
        end

        min_filtered = min_filter(binned_pc, 3)
        for idx ∈ keys(binned_pc)
            # check the min function was applied correctly to each voxel
            @test min_filtered[idx][1] == minimum(map(p -> p.position[3], binned_pc[idx]))
        end
    end

    # now let's test an arbitrary filter function- filter returns 1 if average distance to origin is > 0.1, else 0
    function test_filter(pc::Array)
        norms = []
        for i ∈ 1:length(pc)
            push!(norms, norm.(pc[i].position))
        end
        if mean(norms)[1] < 0.1
            return 0
        else
            return 1
        end
    end

    # filter with 2x2 window
    filtered = ImageFiltering.mapwindow(test_filter, binned_pc, 1)
    for idx ∈ keys(binned_pc)
        if mean(norm.(binned_pc[idx])) > 0.1
            @test filtered[idx][1] == 1
        else
            @test filtered[idx][1] == 0
        end
    end
end

@testset "Test height map from synthetic" begin

    # function generate_height_map_image(x,y,z, width, height, tilesize, tileXmin, tileYmin)
    xmin = 1000
    ymin = 5000
    height_range = 10
    
    tilesize = 100
    metres_per_pixel = 2

    height = width = Int64(ceil(tilesize/metres_per_pixel))

    Random.seed!(42)
    
    # Just generate any heightmap, then create points accordingly, then test the generate_height_map function
    expected_heightmap = height_range .* rand(height, width)
    expected_counts = Int.(floor.( 50 .* rand(height, width)))
    expected_counts[:,1] .= 0  # for test, one column has no points
    idx = findall(expected_counts .== 0)
    expected_heightmap[idx] .= 0

    ymax = ymin + tilesize

    # this will generate a column of points, that would correspond to a pixel in the height map
    function generate_points_in_column(index::CartesianIndex)
        row = index[1]
        column = index[2]

        offset_x = xmin + metres_per_pixel * (column - 1)
        offset_y = ymax - metres_per_pixel * (row)  # north is top-of-image, so index increases from north towards south
        
        max_height = expected_heightmap[index]
        bin_count = expected_counts[index]

        if (bin_count == 0)
            return Vector{Point3}()
        end

        # generate a bunch of bunch below the wanted height for this pixel
        points = map(i -> Vector3( (metres_per_pixel * rand()) + offset_x, (metres_per_pixel * rand()) + offset_y, max_height * rand() ), 1:bin_count)
        # and at least 1 point at exactly that height:
        points[end] = Vector3( (metres_per_pixel * rand()) + offset_x, (metres_per_pixel * rand()) + offset_y, max_height)
        
        return points
    end

    # generate points for all pixels in the expected_heightmap:
    points = map(generate_points_in_column, CartesianIndices(expected_heightmap))

    # and mix them all up:
    points = shuffle(cat(points[:]..., dims=1))
    
    # image = FAFE2.generate_height_map_image(x,y,z, width, height, tilesize, xmin, ymin )
    image, h5map = generate_height_map_image(points, metres_per_pixel, tilesize, xmin, ymin )

    @test size(image) == (height, width)
    @test size(h5map) == (height, width,3)
     
    red_channel = map(p -> p.r, image)
    green_channel = map(p -> p.g, image)
    blue_channel = map(p -> p.b, image)
    
    expected_red_channel = expected_counts
    expected_red_channel = expected_red_channel ./ maximum(expected_red_channel[:])
    expected_red_channel = mapwindow(maximum, expected_red_channel, [3,3])

    expected_green_channel = clamp.(expected_heightmap ./ maximum(HeightMaps.default_max_clip),0,1)
    expected_green_channel = mapwindow(maximum, expected_green_channel, [3,3])
    expected_blue_channel = expected_green_channel .* expected_green_channel
    
    # the image is 8 bit, so the error can be 1/256 off...
    @test maximum(abs.(expected_red_channel[:] .- red_channel[:])) <= (1.0 / 256.0)
    @test maximum(abs.(expected_green_channel[:] .- float.(green_channel[:]))) <= (1.0 / 256.0)
    @test maximum(abs.(expected_blue_channel[:] .- float.(blue_channel[:]))) <= (1.0 / 256.0)

end