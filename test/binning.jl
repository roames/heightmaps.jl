@testset "Binning" begin
    points = map(i -> Point3(rand(3)...), 1:100)
    pc = Table(position = points)
    metres_per_pixel = 1
    num_dims = 2

    @testset "Invalid Inputs" begin
        @test_throws ArgumentError bin_pointcloud(pc, metres_per_pixel; num_dims = 4)
        @test_throws ArgumentError bin_pointcloud(pc, metres_per_pixel; num_dims = 0)
        @test_throws ErrorException bin_pointcloud(pc, metres_per_pixel; num_dims = num_dims, offset = -1e-3)
        @test_throws ErrorException bin_pointcloud(pc, metres_per_pixel; num_dims = num_dims, offset = 0)
        # this will throw an error in the pixel indexing section - for now just check that it does error for this input
        @test_throws Exception bin_pointcloud(pc, 0; num_dims = num_dims)
    end
end