using Dictionaries
using FugroGeometry
using HeightMaps
using Images
using ImageFiltering
using LinearAlgebra
using Random
using Statistics
using Test
using TypedTables

include("binning.jl")
include("maps.jl")
