"""
    construct_pixels(points, metres_per_pixel, bb; kwargs...) -> indices

Map each point in a set of `points` to the CartesianIndex of the pixel it will belong to.

# Arguments
* `points` : Set of points being rasterised
* `metres_per_pixel` : scale of pixel size
* `bb` : Bounding box encompassing `points`

### Keyword Arguments
* `num_dims` : Number of dimensions to rasterise the points over (2D or 3D), default 2
* `offset` : A buffer applied to the boundaries of the image so it contains the full point cloud, default 1e-3
"""
function construct_pixels(points::Vector{SVector{S, T}}, metres_per_pixel::Real, bb::BoundingBox; num_dims::Int64 = 2, offset::Real = 1e-3) where {S, T}
    resolutions = metres_per_pixel * ones(num_dims)

    xsize = bb.xmax - bb.xmin + 2 * offset
    ysize = bb.ymax - bb.ymin + 2 * offset
    zsize = bb.zmax - bb.zmin + 2 * offset

    axes_sizes = [xsize, ysize, zsize]

    # add a buffer so we can include all points
    xmin = bb.xmin - offset
    xmax = bb.xmax + offset
    ymin = bb.ymin - offset
    ymax = bb.ymax + offset
    zmin = bb.zmin - offset
    zmax = bb.zmax + offset

    if num_dims == 2
        zmin = Inf
    end

    axes_bounds = [[xmin, xmax], [ymin, ymax], [zmin, zmax]]

    num_pixels = map(axis -> axes_sizes[axis]/resolutions[axis], 1:num_dims)

    extremal_points = map(axis -> extrema(map(p -> p[axis], points)), 1:num_dims)

    for dim ∈ 1:num_dims
        if (extremal_points[dim][1] < axes_bounds[dim][1]) || (extremal_points[dim][2] >= axes_bounds[dim][2])
            @debug extremal_points[dim]
            @debug axes_bounds[dim]
            throw(ErrorException("Data out of range for dim=$dim !"))
        end
    end

    indices = map(p -> get_pixel_index(p, xmin, ymax, zmin, metres_per_pixel), points)
    return indices
end

""" 
    bin_pointcloud(pc, metres_per_pixel, bb; kwargs...) -> binning

Rasterise a pointcloud into a set of bins given a resolution in either 2 or 3 dimensions

# Arguments
* `pc` : The pointcloud we are discretising
* `metres_per_pixel` : scale of pixel size
* `bb` : Bounding box encompassing the pointcloud region we are discretising

### Keyword Arguments
* `num_dims` : Number of dimensions to rasterise the points over (2D or 3D), default 2
* `offset` : A buffer applied to the boundaries of the image so it contains the full point cloud, default 1e-3
"""
function bin_pointcloud(pc::Table, metres_per_pixel::Real, bb::BoundingBox; num_dims::Int64 = 2, offset::Real = 1e-3)
    if num_dims ∉ [2, 3]
        throw(ArgumentError("Invalid dimensions $num_dims- must be 2D or 3D!"))
    end

    indices = construct_pixels(pc.position, metres_per_pixel, bb; num_dims = num_dims, offset = offset)
    binning = map(i -> view(pc, i), group(indices, 1:length(pc)))

    return binning
end

function bin_pointcloud(pc::Table, metres_per_pixel::Real; num_dims::Int64 = 2, offset::Real = 1e-3)
    bb = boundingbox(pc.position)
    return bin_pointcloud(pc, metres_per_pixel, bb; num_dims = num_dims, offset = offset)
end


""" 
    bin_pointcloud(points, metres_per_pixel, bb; kwargs...) -> binning

Rasterise a vector of points into a set of bins given a resolution in either 2 or 3 dimensions

# Arguments
* `points` : The vector of points being rasterised
* `metres_per_pixel` : scale of pixel size
* `bb` : Bounding box encompassing the pointcloud region we are discretising

### Keyword Arguments
* `num_dims` : Number of dimensions to rasterise the points over (2D or 3D), default 2
* `offset` : A buffer applied to the boundaries of the image so it contains the full point cloud, default 1e-3
"""
function bin_pointcloud(points::Vector{SVector{S, T}}, metres_per_pixel::Real, bb::BoundingBox; num_dims::Int64 = 2, offset::Real = 1e-3) where {S, T}
    if num_dims ∉ [2, 3]
        throw(ArgumentError("Invalid dimensions $num_dims- must be 2D or 3D!"))
    end

    indices = construct_pixels(points, metres_per_pixel, bb; num_dims = num_dims, offset = offset)
    binning = map(i -> view(points, i), group(indices, 1:length(points)))

    return binning
end

function bin_pointcloud(points::Vector{SVector{S, T}}, metres_per_pixel::Real; num_dims::Int64 = 2, offset::Real = 1e-3) where {S, T}
    bb = boundingbox(points)
    return bin_pointcloud(points, metres_per_pixel, bb; num_dims = num_dims, offset = offset)
end

"""
    bin_to_matrix(binned_vals) -> mat

Take a given binned set of values (2D or 3D bins) and present it in matrix format.

# Arguments
* `binned_vals` : Binned values being transformed to a matrix, must be scalar quantities (note these will be stored in a 1-element array in each bin)
"""
function bin_to_matrix(binned_vals::Dictionaries.Dictionary)
    idxs = get_num_axis_bins(binned_vals)
    mat = zeros(idxs...)
    for k ∈ keys(binned_vals)
        mat[k] = binned_vals[k][1]
    end
    return mat
end

"""
    get_num_dims(binned_pc) -> num_dims

Return the number of dimensions a rasterise point cloud is discretised over. 

# Arguments
* `binned_pc` : The binned point cloud being queried
"""
function get_num_dims(binned_pc::Dictionaries.Dictionary)
    keys_array = collect(keys(binned_pc))
    if isa(binned_pc[keys_array[1]], Table)
        num_dims = length(binned_pc[keys_array[1]][1].position)
    else
        num_dims = length(binned_pc[keys_array[1]][1])
    end
    return num_dims
end