module HeightMaps

using Dictionaries
using FugroGeometry
using Images
using ImageMagick
using NearestNeighbors
using SplitApplyCombine
using StaticArrays
using TypedTables

include("defaults.jl")
include("maps.jl")
include("binning.jl")

export generate_height_map_image, generate_height_map, get_extrema, get_voxel_size, get_num_axis_bins
export min_filter, max_filter, density_map, mapwindow, get_filter_neighbours
export bin_pointcloud, bin_to_matrix, get_num_dims

end # module
