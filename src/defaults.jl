currentfolder = dirname(@__FILE__())

const default_denoise_radius = 1.0
const default_image_size = 256
const default_tile_size = 100
const default_save_dir = joinpath(currentfolder, "/heightmaps")

const default_min_clip = 0.5
const default_max_clip = 30
