const PxTransparant = RGBA{Normed{UInt8,8}}
const PxColor = RGB{Normed{UInt8,8}}
const PxGray = Gray{Normed{UInt8,8}}

"""
    denoise_mask(xyz, radius) -> not_noise

Remove any noise points from a set of points.

# Arguments
* `xyz` : Vector of points being filtered
* `radius` : Threshold radius in which any non-noise points must have at least one nearest neighbour
"""
function denoise_mask(xyz::Vector{SVector{S, T}}, radius::Float64) where {S, T}
 
    kdtree = KDTree(xyz)
    distance_to_nn = map(p -> knn(kdtree, p, 2, true)[2][2], xyz)
    not_noise = distance_to_nn .< radius
    
    return not_noise
end

denoise_mask(pc::Table, radius::Float64) = denoise_mask(pc.position, radius)

"""
    get_pixel_index(point, x_min, y_max, z_min, metres_per_pixel)

Get the index of a point in a rasterised grid. 

# Arguments
* `point` : Point being queried
* `x_min` : Minimum x coordinate in the grid
* `y_max` : Maximum y coordinate in the grid
* `z_min` : Minimum z coordinate in the grid
* `metres_per_pixel` : Scale of metres to each pixel edge size
"""
function get_pixel_index(point::SVector{S, T}, x_min::Real, y_max::Real, z_min::Real, metres_per_pixel::Real) where {S, T}
    if metres_per_pixel <= 0
        throw(ArgumentError("Cannot have negative or zero metres per pixel!"))
    end
    dx = point[1] - x_min
    dy = y_max - point[2]

    x_idx = Int(floor(dx/metres_per_pixel)) + 1
    y_idx = Int(ceil(dy/metres_per_pixel))

    
    if !isinf(z_min) && (length(point) == 3)
        dz = point[3] - z_min
        z_idx = Int(floor(dz/metres_per_pixel)) + 1
        return CartesianIndex(y_idx, x_idx, z_idx)
    end

    return CartesianIndex(y_idx, x_idx)
end

get_pixel_index(point::SVector{S, T}, xmin::Real, ymax::Real, metres_per_pixel::Real) where {S, T} = get_pixel_index(point, xmin, ymax, Inf, metres_per_pixel)

function get_count(points::Vector{SVector{S, T}}) where {S, T}
    return length(points)
end

"""
    Return the maximum z-coordinate in a vector of points 
"""
function get_max_height(points::Vector{SVector{S, T}}) where {S, T}
    return maximum(p -> p[3], points)
end

get_max_height(pc::Table) = get_max_height(pc.position)
get_max_height(pcs::Array) = maximum(pc -> get_max_height(pc), pcs)

"""
    Return the minimum z-coordinate in a vector of points 
"""
function get_min_height(points::Vector{SVector{S, T}}) where {S, T}
    return minimum(p -> p[3], points)
end

get_min_height(pc::Table) = get_min_height(pc.position)
get_min_height(pcs::Array) = minimum(pc -> get_min_height(pc), pcs)

function combine_channels(red::Array, green::Array, blue::Array)
    combined = Float32.(cat(red, green, blue, dims=3))
    return combined
end


function generate_height_map_image(points::Vector{SVector{S, T}}, metres_per_pixel::Real, tilesize::Real, tileXmin::Real, tileYmin::Real) where {S, T}
    tileYmax = tileYmin + tilesize
    tileXmax = tileXmin + tilesize

    height = width = Int64(ceil(tilesize/metres_per_pixel))

    in_range = map(p -> (p[1] >= tileXmin) && (p[1] < tileXmax) && (p[2] >= tileYmin) && (p[2] < tileYmax), points)

    points = points[in_range]

    out_of_range = count(.!in_range)
    
    if out_of_range > 10
        @warn "$(out_of_range) out of $(length(points)) points were clipped off when generating the height map."
    end

    if length(points) == 0
        @warn "No points... height map will be black..."
    end

    indices = map(p -> get_pixel_index(p, tileXmin, tileYmax, metres_per_pixel), points)
    binning = group(indices, points)
    
    pixels = collect(keys(binning))
    content = collect(binning)

    red = zeros(height, width)
    red[pixels] .= map(get_count, content)
    red = red ./ maximum(red)
    red = mapwindow(maximum, red, [3,3])

    green = zeros(height, width)
    green[pixels] .= map(get_max_height, content)
    green = clamp.(green ./ maximum(default_max_clip), 0, 1.0)
    green = mapwindow(maximum, green, [3,3])

    blue = green .* green
    
    image = map( c -> PxColor(c[1], c[2], c[3]), zip(red, green, blue))

    return image, combine_channels(red, green, blue)
end


function generate_height_map(pc::Table, tileXmin::Real, tileYmin::Real, tile_name::Real; image_width::Real = default_image_size, image_height::Real =  default_image_size, tile_size::Real = default_tile_size, save_dir::AbstractString = default_save_dir)

    xyz = pc.position
    agl = pc.agl
    
    @info "denoise_pc"
    not_noise = denoise_mask(xyz, default_denoise_radius)
    
    x = [ i[1] for i in xyz[not_noise] ]
    y = [ i[2] for i in xyz[not_noise] ]
    agl = agl[not_noise]

    points = map(i -> SVector(x[i], y[i], agl[i]), 1:length(agl))

    @info "generate_height_map_image"
    hmap, h5hmap = generate_height_map_image(points, image_width, image_height, tile_size, tileXmin, tileYmin)
    
    filename = joinpath(save_dir, tile_name * ".png")
    
    mkpath(dirname(filename))
    Images.save(filename, hmap)

    h5filename = joinpath(save_dir, tile_name * ".h5")
    h5write(h5filename, "HMAP", h5hmap)

    @info "Tile $filename DONE!!!"
    
    return filename
end

"""
    get_voxel_size(binned_pc) -> voxel_size

Get the volume or area of a voxel in a binned point cloud.

# Arguments
* `binned_pc` : The binned point cloud being queried
"""
function get_voxel_size(binned_pc::Dictionaries.Dictionary)
    axes_lengths = get_num_axis_bins(binned_pc)
    num_dims = get_num_dims(binned_pc)
    axes_extrema = map(axis -> get_extrema(binned_pc, axis), 1:num_dims)
    axes_dist = map(axis -> (axes_extrema[axis][2] - axes_extrema[axis][1])/axes_lengths[axis], 1:num_dims)
    
    voxel_size = prod(axes_dist)

    return voxel_size
end

"""
    get_extrema(binned_pc, dim) -> global_min, global_max

Get the extreme points of a binned point cloud along a given dimension.

# Arguments
* `binned_pc` : The binned point cloud being queried
* `dim` : Dimension of coordinates being examined
"""
function get_extrema(binned_pc::Dictionaries.Dictionary, dim::Int64)
    num_dims = get_num_dims(binned_pc)
    if dim > num_dims
        throw(ArgumentError("Invalid dimension dim=$dim !"))
    end
    min_vals = min_filter(binned_pc, dim)
    max_vals = max_filter(binned_pc, dim)
    global_min = minimum(map(k -> min_vals[k], keys(min_vals)))[1]
    global_max = maximum(map(k -> max_vals[k], keys(max_vals)))[1]

    return global_min, global_max
    
end

"""
    get_num_axis_bins(binned_pc)

Return the number of bins along each dimension of a binned point cloud.

# Arguments
* `binned_pc` : The binned point cloud being queried
"""
function get_num_axis_bins(binned_pc::Dictionaries.Dictionary)
    keys_array = collect(keys(binned_pc))
    num_dims = length(keys_array[1])
    xmax_idx = maximum(map(k -> k[1], keys_array))
    ymax_idx = maximum(map(k -> k[2], keys_array))
    if num_dims == 2
        return xmax_idx, ymax_idx
    end

    zmax_idx = maximum(map(k -> k[3], keys_array))
    return xmax_idx, ymax_idx, zmax_idx
    
end

"""
    get_filter_neighbours(idx, binned_pc, bin_sizes, window_size)

Get the neighbouring voxels of a pixel given a filter window.

# Arguments
* `idx` : Pixel index being queried
* `binned_pc` : The binned point cloud containing the pixel
* `bin_sizes` : Number of bins along each axis of the binned pointcloud
* `window_size` : Size of the filter (equal along each dimension)
"""
function get_filter_neighbours(idx::CartesianIndex, binned_pc::Dictionaries.Dictionary, bin_sizes::Tuple, window_size::Int64)
    num_axes_bins = Int(round((window_size - 1)/2))
    local_coords = Dict(i => [j for j ∈ -num_axes_bins:num_axes_bins if (j + idx[i]) ∈ 1:bin_sizes[i]] for i ∈ 1:length(bin_sizes))

    local_idxs = []
    if length(bin_sizes) == 2
        local_idxs = [CartesianIndex(i, j) for i ∈ local_coords[1] for j ∈ local_coords[2]]
    else
        local_idxs = [CartesianIndex(i, j, k) for i ∈ local_coords[1] for j ∈ local_coords[2] for k ∈ local_coords[3]]
    end
    return [idx + neigh_idx for neigh_idx ∈ local_idxs if idx + neigh_idx ∈ keys(binned_pc)]
end

"""
    Apply a function `f` to a binned point cloud `binned_pc` with a filter size of `window_size`
"""
function ImageFiltering.mapwindow(f::Function, binned_pc::Dictionaries.Dictionary, window_size::Int64)
    bin_sizes = get_num_axis_bins(binned_pc)
    neighbour_idxs = Dict(idx => get_filter_neighbours(idx, binned_pc, bin_sizes, window_size) for idx ∈ keys(binned_pc))
    map_res = zeros(bin_sizes)
    for idx ∈ keys(binned_pc)
        neigh_vals = map(n -> binned_pc[n], neighbour_idxs[idx])
        if binned_pc[idx] ∉ neigh_vals
            push!(neigh_vals, binned_pc[idx])
        end
        map_res[idx] = f(neigh_vals)
    end
    new_binning = group(collect(keys(map_res)), map_res)

    return new_binning
end

"""
    min_filter(binned_pc, dim)

Apply a minimum filter over the values in a given dimension of a binned point cloud locally to each pixel

# Arguments
* `binned_pc` : The binned point cloud being filtered
* `dim` : Dimension along which values are filtered
"""
function min_filter(binned_pc::Dictionaries.Dictionary, dim::Int64)
    num_dims = get_num_dims(binned_pc)
    if dim > num_dims
        throw(ArgumentError("Invalid dimension dim=$dim !"))
    end
    min_func(pc::Array) = minimum(map(i -> minimum(map(p -> p.position[dim], pc[i])), 1:length(pc)))
    return ImageFiltering.mapwindow(min_func, binned_pc, 1)
end

"""
    max_filter(binned_pc, dim)

Apply a maximum filter over the values in a given dimension of a binned point cloud locally to each pixel

# Arguments
* `binned_pc` : The binned point cloud being filtered
* `dim` : Dimension along which values are filtered
"""
function max_filter(binned_pc::Dictionaries.Dictionary, dim::Int64)
    num_dims = get_num_dims(binned_pc)
    if dim > num_dims
        throw(ArgumentError("Invalid dimension dim=$dim !"))
    end
    max_func(pc::Array) = maximum(map(i -> maximum(map(p -> p.position[dim], pc[i])), 1:length(pc)))
    return ImageFiltering.mapwindow(max_func, binned_pc, 1)
end

function max_filter(binned_pc::Dictionaries.Dictionary, column::Symbol)
    max_field_func(pcs::Array) = maximum(map(i -> maximum(map(p -> getproperty(p, column), pcs[i])), 1:length(pcs)))
    return ImageFiltering.mapwindow(max_field_func, binned_pc, 1)
end

function min_filter(binned_pc::Dictionaries.Dictionary, column::Symbol)
    min_field_func(pcs::Array) = minimum(map(i -> minimum(map(p -> getproperty(p, column), pcs[i])), 1:length(pcs)))
    return ImageFiltering.mapwindow(min_field_func, binned_pc, 1)
end